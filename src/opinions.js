export default {
    yes:      { value: 3, bold: true },
    jokingly: { value: 1 },
    close:    { value: 1 },
    meh:      { value: 0 },
    nope:     { value: -3, small: true, color: 'muted' },
};
